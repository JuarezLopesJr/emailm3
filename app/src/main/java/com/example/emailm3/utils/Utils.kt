package com.example.emailm3.utils

import com.example.emailm3.data.Email

object EmailFactory {
    fun makeEmailList() = listOf(
        Email(
            "1",
            "Mussum Ipsum, cacilds",
            "osuere libero varius. Nullam a nisl ut ante blandit hendrerit. " +
                    "Aenean sit ame" +
                    " Quem num gosta di mé, boa gentis num é"
        ),
        Email(
            "2",
            "Quem num gosta di mim que vai caçá sua turmis!",
            "Viva Forevis aptent taciti sociosqu ad litora torquent." +
                    "Per aumento de cachacis, eu reclamis.Praesent malesuada urna nisi, " +
                    "quis volutpat erat hendrerit non."
        ),
        Email(
            "3",
            "Casamentiss faiz malandris se pirulitá",
            "Quem manda na minha terra sou euzis!" +
                    "A ordem dos tratores não altera o pão duris"
        ),
        Email(
            "4",
            "Mé faiz elementum girarzis",
            "Manduma pindureta quium dia nois paga." +
                    "Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis"
        ),
        Email(
            "5",
            "Mais vale um bebadis conhecidiss, que um alcoolatra anonimis",
            "Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis" +
                    "Suco de cevadiss deixa as pessoas mais interessantis. "
        ),
        Email(
            "6",
            "Leite de mula manquis sem cabeça",
            "Todo mundo vê os porris que eu tomo, mas ninguém vê os tombis que eu levo" +
                    "Cevadis im ampola pa arma uma pindureta" +
                    "Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis"
        )
    )
}

object Tags {
    const val TAG_EMAIL = "email_"
    const val TAG_EMAIL_LIST = "email_list"
    const val TAG_CONTENT = "content"
    const val TAG_PROGRESS = "progress"
    const val TAG_EMPTY = "empty"
    const val TAG_ERROR = "error"
}