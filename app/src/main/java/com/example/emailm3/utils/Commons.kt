package com.example.emailm3.utils

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun HandleInboxEvent(
    modifier: Modifier,
    eventDescription: String,
    buttonLabel: String,
    handleEventListener: () -> Unit
) {
    Column(
        modifier = modifier.padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(text = eventDescription)

        Spacer(modifier = Modifier.height(12.dp))

        Button(onClick = handleEventListener) {
            Text(text = buttonLabel)
        }
    }
}