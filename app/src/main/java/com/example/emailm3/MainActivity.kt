package com.example.emailm3

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.example.emailm3.ui.screen.inbox.InboxScreen
import com.example.emailm3.ui.theme.EmailM3Theme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            EmailM3Theme {
                // A surface container using the 'background' color from the theme
                InboxScreen()
            }
        }
    }
}