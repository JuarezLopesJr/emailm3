package com.example.emailm3.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.emailm3.data.InboxEvent
import com.example.emailm3.data.InboxState
import com.example.emailm3.data.InboxStatus
import com.example.emailm3.utils.EmailFactory
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class InboxViewModel : ViewModel() {
    private val _uiState = MutableStateFlow(InboxState())
    val uiState = _uiState.asStateFlow()

    private fun deleteEmail(id: String) {
        _uiState.value = _uiState.value.copy(
            emails = EmailFactory.makeEmailList().filter {
                it.id != id
            }
        )
    }

    fun loadContent() {
        viewModelScope.launch {
            _uiState.value = _uiState.value.copy(status = InboxStatus.LOADING)

            delay(3000L)

            _uiState.value = _uiState.value.copy(
                status = InboxStatus.SUCCESS,
                emails = EmailFactory.makeEmailList()
            )
        }
    }

    fun handleEvent(event: InboxEvent) {
        when (event) {
            is InboxEvent.RefreshContent -> loadContent()
            is InboxEvent.DeleteEmail -> deleteEmail(event.id)
        }
    }
}