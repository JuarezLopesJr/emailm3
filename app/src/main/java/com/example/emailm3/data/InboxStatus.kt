package com.example.emailm3.data

enum class InboxStatus {
    LOADING, SUCCESS, ERROR, EMPTY
}