package com.example.emailm3.data

data class InboxState(
    val status: InboxStatus = InboxStatus.LOADING,
    val emails: List<Email> = emptyList()
)