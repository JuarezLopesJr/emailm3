package com.example.emailm3.data

sealed class InboxEvent {
    object RefreshContent : InboxEvent()

    class DeleteEmail(val id: String) : InboxEvent()
}