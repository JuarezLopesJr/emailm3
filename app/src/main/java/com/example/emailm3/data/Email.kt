package com.example.emailm3.data

data class Email(
    val id: String,
    val title: String,
    val description: String
)