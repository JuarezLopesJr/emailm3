@file:OptIn(
    ExperimentalMaterialApi::class, ExperimentalMaterialApi::class,
    ExperimentalMaterialApi::class, ExperimentalMaterialApi::class
)

package com.example.emailm3.ui.screen.email

import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.DismissDirection
import androidx.compose.material.DismissState
import androidx.compose.material.DismissValue
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.FractionalThreshold
import androidx.compose.material.SwipeToDismiss
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.rememberDismissState
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.scale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.CustomAccessibilityAction
import androidx.compose.ui.semantics.customActions
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.emailm3.R
import com.example.emailm3.data.Email
import com.example.emailm3.data.InboxEvent
import com.example.emailm3.utils.Tags
import com.example.emailm3.utils.Tags.TAG_CONTENT
import com.example.emailm3.utils.Tags.TAG_EMAIL

@Composable
fun EmailListScreen(
    modifier: Modifier = Modifier,
    emails: List<Email>,
    inboxEventListener: (InboxEvent) -> Unit
) {
    LazyColumn(modifier = modifier.testTag(TAG_CONTENT)) {
        items(emails, key = { it.id }) { email ->
            val deleteEmailLabel = stringResource(id = R.string.cd_delete_email)

            var isEmailItemDismissed by remember { mutableStateOf(false) }

            val emailHeightAnimation by animateDpAsState(
                targetValue = if (isEmailItemDismissed) 0.dp else 120.dp,
                animationSpec = tween(delayMillis = 300),
                finishedListener = {
                    inboxEventListener(InboxEvent.DeleteEmail(email.id))
                }
            )

            val dismissState = rememberDismissState(
                confirmStateChange = {
                    if (it == DismissValue.DismissedToEnd) {
                        isEmailItemDismissed = !isEmailItemDismissed
                    }
                    it != DismissValue.DismissedToEnd
                }
            )

            val dividerVisibilityAnimation by animateFloatAsState(
                targetValue = if (dismissState.targetValue == DismissValue.Default) {
                    1f
                } else 0f,
                animationSpec = tween(delayMillis = 300)
            )

            /* configure custom actions to hook into talkback api allowing the user
               to delete an email via the native accessibility services */
            SwipeToDismiss(
                modifier = Modifier
                    .testTag(TAG_EMAIL + email.id)
                    .semantics(mergeDescendants = true) {
                        /* this handler needs to return a boolean flag
                       to indicate whether the action was handled */
                        customActions = listOf(
                            CustomAccessibilityAction(deleteEmailLabel) {
                                inboxEventListener(InboxEvent.DeleteEmail(email.id))
                                true
                            }
                        )
                    },
                state = dismissState,
                directions = setOf(DismissDirection.StartToEnd),
                dismissThresholds = { FractionalThreshold(0.20f) },
                dismissContent = {
                    EmailItem(
                        modifier = Modifier
                            .testTag(Tags.TAG_EMAIL_LIST)
                            .fillMaxWidth()
                            .height(emailHeightAnimation),
                        email = email,
                        dismissState = dismissState
                    )
                },
                background = { EmailItemBackground(dismissState = dismissState) }
            )

            Divider(
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .alpha(dividerVisibilityAnimation)
            )
        }
    }
}

@Composable
private fun EmailItem(
    modifier: Modifier = Modifier,
    email: Email,
    dismissState: DismissState
) {

    val cardElevation = animateDpAsState(
        targetValue = if (dismissState.dismissDirection != null) 4.dp else 0.dp
    ).value

    Card(
        modifier = modifier
            .padding(16.dp)
            .fillMaxSize(),
        elevation = CardDefaults.cardElevation(cardElevation)
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth()
        ) {
            Text(text = email.title, fontWeight = FontWeight.Bold)

            Spacer(modifier = Modifier.height(8.dp))

            Text(
                text = email.description,
                fontSize = 14.sp,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

@Composable
private fun EmailItemBackground(
    modifier: Modifier = Modifier,
    dismissState: DismissState
) {
    val backgroundColor by animateColorAsState(
        targetValue = when (dismissState.targetValue) {
            DismissValue.DismissedToEnd -> MaterialTheme.colorScheme.error
            else -> MaterialTheme.colorScheme.background
        },
        animationSpec = tween()
    )

    val iconColor by animateColorAsState(
        targetValue = when (dismissState.targetValue) {
            DismissValue.DismissedToEnd -> MaterialTheme.colorScheme.onError
            else -> MaterialTheme.colorScheme.onSurface
        },
        animationSpec = tween()
    )

    val iconScale by animateFloatAsState(
        targetValue = if (dismissState.targetValue == DismissValue.DismissedToEnd) 1f else 0.75f
    )

    Box(
        modifier = modifier
            .background(backgroundColor)
            .fillMaxSize()
            .padding(horizontal = 20.dp)
    ) {
        if (dismissState.currentValue == DismissValue.Default) {
            Icon(
                modifier = Modifier
                    .align(Alignment.CenterStart)
                    .scale(iconScale),
                imageVector = Icons.Default.Delete,
                tint = iconColor,
                contentDescription = stringResource(id = R.string.cd_delete_email)
            )
        }
    }
}