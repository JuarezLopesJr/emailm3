package com.example.emailm3.ui.screen.error

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.example.emailm3.R
import com.example.emailm3.data.InboxEvent
import com.example.emailm3.utils.HandleInboxEvent

@Composable
fun ErrorScreen(
    modifier: Modifier = Modifier,
    inboxEventListener: (InboxEvent) -> Unit
) {
    HandleInboxEvent(
        modifier = modifier,
        eventDescription = stringResource(id = R.string.message_content_error),
        buttonLabel = stringResource(id = R.string.label_try_again),
    ) {
        inboxEventListener(InboxEvent.RefreshContent)
    }
}