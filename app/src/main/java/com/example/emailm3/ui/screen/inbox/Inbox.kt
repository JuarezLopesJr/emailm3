@file:OptIn(
    ExperimentalLifecycleComposeApi::class, ExperimentalMaterial3Api::class,
    ExperimentalLifecycleComposeApi::class
)

package com.example.emailm3.ui.screen.inbox

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.emailm3.R
import com.example.emailm3.data.InboxEvent
import com.example.emailm3.data.InboxState
import com.example.emailm3.data.InboxStatus
import com.example.emailm3.ui.screen.email.EmailListScreen
import com.example.emailm3.ui.screen.empty.EmptyScreen
import com.example.emailm3.ui.screen.error.ErrorScreen
import com.example.emailm3.utils.Tags.TAG_CONTENT
import com.example.emailm3.utils.Tags.TAG_EMPTY
import com.example.emailm3.utils.Tags.TAG_ERROR
import com.example.emailm3.utils.Tags.TAG_PROGRESS
import com.example.emailm3.viewmodel.InboxViewModel

@Composable
fun InboxScreen(modifier: Modifier = Modifier) {
    val viewModel = viewModel<InboxViewModel>()
    val inboxState by viewModel.uiState.collectAsStateWithLifecycle()

    LaunchedEffect(Unit) {
        viewModel.loadContent()
    }

    EmailInbox(
        modifier = modifier.fillMaxWidth(),
        inboxState = inboxState,
        inboxEventListener = viewModel::handleEvent
    )
}

@Composable
fun EmailInbox(
    modifier: Modifier = Modifier,
    inboxState: InboxState,
    inboxEventListener: (InboxEvent) -> Unit
) {
    Scaffold(
        modifier = modifier,
        topBar = {
            CenterAlignedTopAppBar(
                title = {
                    Text(
                        text = stringResource(R.string.title_inbox, inboxState.emails.size),
                        fontWeight = FontWeight.Bold
                    )
                }
            )
        },
        content = {
            Box(
                modifier = Modifier
                    .padding(paddingValues = it)
                    .fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                when (inboxState.status) {
                    InboxStatus.LOADING -> {
                        CircularProgressIndicator(modifier = Modifier.testTag(TAG_PROGRESS))
                    }
                    InboxStatus.ERROR -> {
                        ErrorScreen(
                            modifier = Modifier.testTag(TAG_ERROR),
                            inboxEventListener = inboxEventListener
                        )
                    }
                    InboxStatus.EMPTY -> {
                        EmptyScreen(
                            modifier = Modifier.testTag(TAG_EMPTY),
                            inboxEventListener = inboxEventListener
                        )
                    }
                    InboxStatus.SUCCESS -> {
                        EmailListScreen(
                            modifier = Modifier.testTag(TAG_CONTENT),
                            emails = inboxState.emails,
                            inboxEventListener = inboxEventListener
                        )
                    }
                }
            }
        }
    )
}