package com.example.emailm3

import androidx.annotation.StringRes
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.test.platform.app.InstrumentationRegistry
import com.example.emailm3.data.InboxState
import com.example.emailm3.data.InboxStatus
import com.example.emailm3.ui.screen.inbox.EmailInbox
import com.example.emailm3.utils.EmailFactory
import com.example.emailm3.utils.Tags.TAG_CONTENT
import com.example.emailm3.utils.Tags.TAG_EMPTY
import com.example.emailm3.utils.Tags.TAG_ERROR
import com.example.emailm3.utils.Tags.TAG_PROGRESS
import org.junit.Rule
import org.junit.Test

class EmailInboxTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    private fun getComposeString(@StringRes id: Int, format: Int = 0): String {
        return InstrumentationRegistry.getInstrumentation().targetContext.getString(id, format)
    }

    @Test
    fun assert_Inbox_Title_Displayed() {
        val inboxState = InboxState(emails = EmailFactory.makeEmailList())

        composeTestRule.setContent {
            EmailInbox(inboxState = inboxState, inboxEventListener = {})
        }

        composeTestRule.onNodeWithText(
            getComposeString(
                R.string.title_inbox,
                inboxState.emails.count()
            )
        ).assertIsDisplayed()
    }

    @Test
    fun assert_Loading_State_Displayed() {
        composeTestRule.apply {
            setContent {
                EmailInbox(
                    inboxState = InboxState(status = InboxStatus.LOADING),
                    inboxEventListener = {}
                )
            }

            onNodeWithTag(TAG_PROGRESS).assertIsDisplayed()
            onNodeWithTag(TAG_CONTENT).assertDoesNotExist()
            onNodeWithTag(TAG_EMPTY).assertDoesNotExist()
            onNodeWithTag(TAG_ERROR).assertDoesNotExist()
        }
    }

    @Test
    fun assert_Content_State_Displayed() {
        composeTestRule.apply {
            setContent {
                EmailInbox(
                    inboxState = InboxState(status = InboxStatus.SUCCESS),
                    inboxEventListener = {}
                )
            }

            onNodeWithTag(TAG_CONTENT).assertExists()
            onNodeWithTag(TAG_PROGRESS).assertDoesNotExist()
            onNodeWithTag(TAG_EMPTY).assertDoesNotExist()
            onNodeWithTag(TAG_ERROR).assertDoesNotExist()
        }
    }

    @Test
    fun assert_Empty_State_Displayed() {
        composeTestRule.apply {
            setContent {
                EmailInbox(
                    inboxState = InboxState(status = InboxStatus.EMPTY),
                    inboxEventListener = {}
                )
            }

            onNodeWithTag(TAG_EMPTY).assertIsDisplayed()
            onNodeWithTag(TAG_PROGRESS).assertDoesNotExist()
            onNodeWithTag(TAG_CONTENT).assertDoesNotExist()
            onNodeWithTag(TAG_ERROR).assertDoesNotExist()
        }
    }

    @Test
    fun assert_Error_State_Displayed() {
        composeTestRule.apply {
            setContent {
                EmailInbox(
                    inboxState = InboxState(status = InboxStatus.ERROR),
                    inboxEventListener = {}
                )
            }

            onNodeWithTag(TAG_ERROR).assertIsDisplayed()
            onNodeWithTag(TAG_PROGRESS).assertDoesNotExist()
            onNodeWithTag(TAG_CONTENT).assertDoesNotExist()
            onNodeWithTag(TAG_EMPTY).assertDoesNotExist()
        }
    }
}