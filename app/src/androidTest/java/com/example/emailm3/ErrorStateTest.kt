package com.example.emailm3

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.platform.app.InstrumentationRegistry
import com.example.emailm3.data.InboxEvent
import com.example.emailm3.ui.screen.error.ErrorScreen
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class ErrorStateTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Dismiss_Error_Triggered() {
        val dismissState: (InboxEvent) -> Unit = mock()

        composeTestRule.setContent {
            ErrorScreen(inboxEventListener = dismissState)
        }

        composeTestRule.onNodeWithText(
            InstrumentationRegistry.getInstrumentation().targetContext
                .getString(R.string.label_try_again)
        ).performClick()

        verify(dismissState).invoke(InboxEvent.RefreshContent)
    }
}