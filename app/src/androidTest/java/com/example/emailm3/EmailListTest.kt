package com.example.emailm3

import androidx.compose.ui.test.assert
import androidx.compose.ui.test.hasTestTag
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performScrollTo
import androidx.compose.ui.test.performTouchInput
import androidx.compose.ui.test.swipeRight
import com.example.emailm3.data.InboxEvent
import com.example.emailm3.ui.screen.email.EmailListScreen
import com.example.emailm3.utils.EmailFactory
import com.example.emailm3.utils.Tags.TAG_CONTENT
import com.example.emailm3.utils.Tags.TAG_EMAIL
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.refEq
import org.mockito.kotlin.verify

class EmailListTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    val emails = EmailFactory.makeEmailList()

    @Test
    fun assert_Emails_Items_Displayed() {
        composeTestRule.setContent {
            EmailListScreen(emails = emails, inboxEventListener = {})
        }

        emails.forEachIndexed { index, email ->
            composeTestRule.onNodeWithTag(TAG_CONTENT)
                .onChildAt(index)
                .performScrollTo()
                .assert(hasTestTag(TAG_EMAIL + email.id))
        }
    }

    @Test
    fun assert_Delete_Email_Triggered() {
        val deleteEmail: (InboxEvent) -> Unit = mock()
        val indexToDelete = 2

        composeTestRule.setContent {
            EmailListScreen(
                emails = emails,
                inboxEventListener = deleteEmail
            )
        }

        composeTestRule.onNodeWithTag(TAG_CONTENT)
            .onChildAt(indexToDelete)
            .performScrollTo()
            .performTouchInput {
                swipeRight()
            }

        /* this is because the inboxEventListener is only triggered after the animation completes
         in this case: val emailHeightAnimation by animateDpAsState(..., finishedListener = {
                    inboxEventListener(InboxEvent.DeleteEmail(email.id))
                }) */
        composeTestRule.waitForIdle()

        /* refEq is to make work with mockito */
        verify(deleteEmail).invoke(refEq(InboxEvent.DeleteEmail(emails[indexToDelete].id)))
    }
}