package com.example.emailm3

import androidx.compose.ui.test.assert
import androidx.compose.ui.test.assertCountEquals
import androidx.compose.ui.test.hasTestTag
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onChildren
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performScrollTo
import androidx.compose.ui.test.performTouchInput
import androidx.compose.ui.test.swipeRight
import com.example.emailm3.ui.screen.inbox.InboxScreen
import com.example.emailm3.utils.EmailFactory
import com.example.emailm3.utils.Tags.TAG_CONTENT
import com.example.emailm3.utils.Tags.TAG_EMAIL
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class InboxActionsTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    val emails = EmailFactory.makeEmailList()

    @Before
    fun setComposeTestRule() {
        composeTestRule.setContent { InboxScreen() }

        composeTestRule.onNodeWithTag(TAG_CONTENT)
            .onChildAt(0)
            .performTouchInput {
                swipeRight()
            }
    }

    @Test
    fun assert_Item_Dismissed_When_Swiped() {
        composeTestRule.onNodeWithTag(TAG_CONTENT)
            .onChildren()
            .assertCountEquals(emails.count() - 1)
    }

    @Test
    fun assert_Remaining_Items_Displayed_When_Another_Dismissed() {
        emails.takeLast(emails.count() - 1).forEachIndexed { index, email ->
            composeTestRule.onNodeWithTag(TAG_CONTENT)
                .onChildAt(index)
                .performScrollTo()
                .assert(hasTestTag(TAG_EMAIL + email.id))
        }
    }
}