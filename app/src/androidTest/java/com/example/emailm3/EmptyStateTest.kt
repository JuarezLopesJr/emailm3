package com.example.emailm3

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.platform.app.InstrumentationRegistry
import com.example.emailm3.data.InboxEvent
import com.example.emailm3.ui.screen.empty.EmptyScreen
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class EmptyStateTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun assert_Refresh_Triggered() {
        val refreshState: (InboxEvent) -> Unit = mock()

        composeTestRule.setContent {
            EmptyScreen(inboxEventListener = refreshState)
        }

        composeTestRule.onNodeWithText(
            InstrumentationRegistry.getInstrumentation().targetContext
                .getString(R.string.label_check_again)
        ).performClick()

        verify(refreshState).invoke(InboxEvent.RefreshContent)
    }
}